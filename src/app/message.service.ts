import { Injectable } from '@angular/core';
import { Message } from "../app/message.model";

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor() { }

  getAllMessages(): Message[] {

    return [
    new Message("Carrie Fisher", "https://picsum.photos/60", "Instant gratification takes too long"),
    new Message("Confucius", "https://picsum.photos/61", "Man who make mistake in elevator, wrong on many levels"),
    new Message("Al Pacino", "https://picsum.photos/62", "Shakespearse's plays are more violent than 'Scarface'"),
    new Message("Groucho Marx", "https://picsum.photos/63", "I refuse to join any club that would have me as a member"),
    new Message("Samuel L. Jackson", "https://picsum.photos/64", "Given that it's a stupid-ass decision, I've elected to ignore it"),
    new Message("Clint Eastwood", "https://picsum.photos/65", "I tried being reasonable, I didn't like it")
    ];
  }
}
