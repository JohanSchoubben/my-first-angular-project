
export class Message {
    private author: string;
    private messageAvatar: string;
    private messageBody: string;

    constructor(author: string, messageAvatar: string, messageBody: string) {
        this.author = author;
        this.messageAvatar = messageAvatar;
        this.messageBody = messageBody;
    }

    public getAuthor(): string {
        return this.author;
    }
    public getMessageAvatar(): string {
        return this.messageAvatar;
    }
    public getMessageBody(): string {
        return this.messageBody;
    }
}

