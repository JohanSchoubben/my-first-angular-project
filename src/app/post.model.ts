export class Post {
    private heading: string;
    private body: string;
    private avatar: string;

    constructor(heading: string, body: string, avatar: string) {
        this.heading = heading;
        this.body = body;
        this.avatar = avatar;
    }

    public getHeading(): string {
        return this.heading;
    }
    public getBody(): string {
        return this.body;
    }
    public getAvatar(): string {
        return this.avatar;
    }
}
