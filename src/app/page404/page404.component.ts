import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page404',
  templateUrl: './page404.component.html',
  styleUrls: ['./page404.component.css']
})
export class Page404Component implements OnInit {
  page404Title: string = "This feature is under construction";
  page404Text: string = "More functions and content will be added soon";
  page404Image:string = "https://images.unsplash.com/photo-1558691518-331f8672f840?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=689&q=80";
  imageAlt:string = "sunset-behind-construction-site;"

  constructor() { }

  ngOnInit(): void {
  }

}
