import { Component, OnInit } from '@angular/core';
import { Post } from '../post.model';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {
  blogTitle:string = "Some kind of blog";
  blogText:string = "";
  newPost: Post = new Post("A new post", "Non bis in idem", "https://picsum.photos/82");
  constructor() { }

  ngOnInit(): void {
  }

}
