
import { Component, OnInit } from '@angular/core';
import { Message } from "../message.model";

@Component({
  selector: 'app-aside',
  templateUrl: './aside.component.html',
  styleUrls: ['./aside.component.css']
})
export class AsideComponent implements OnInit {

  showMessages: boolean = true;

  constructor() { }

  ngOnInit(): void {
  }

  toggleShowMessages(): void {
    this.showMessages = !this.showMessages;
    // if (!this.showMessages) {
    //   this.showMessages = true;
    // } else {
    //   this.showMessages = false;
    // }
  }

}
