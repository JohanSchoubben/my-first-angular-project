import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.css']
})
export class IntroComponent implements OnInit {
  projectTitle:string = "This is my first ever Angular project";
  projectImage:string = "https://picsum.photos/700/300";
  projectText:string = "Two weeks ago, I had never made a website. Now, I'm doing this ...";
  badgeText:string = "16/03/2021";

  constructor() { }

  ngOnInit(): void {
  }

}
